﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NetCoreAPITemplate.Core.Domain.ComponentTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetCoreAPITemplate.Data.Configuration.ComponentTypes
{
    public class ProductTypeConfiguration : IEntityTypeConfiguration<ProductType>
    {
        public void Configure(EntityTypeBuilder<ProductType> builder)
        {
        }
    }
}
