﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetCoreAPITemplate.Core.Domain.ComponentTypes
{
    public class ProductType : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
