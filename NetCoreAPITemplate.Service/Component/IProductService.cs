﻿using NetCoreAPITemplate.Core.Domain.Components;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetCoreAPITemplate.Service.Component
{
    public interface IProductService
    {
        IEnumerable<Product> GetProducts();
    }
}
